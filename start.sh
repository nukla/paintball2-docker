#!/usr/bin/env bash
# Small script that copies some files over and starts the paintball2 server

PBALLDIR="/paintball2/pball"
INTERNALPBALLDIR="/home/dpserver/pball"

if [ -z "$(ls -A "$PBALLDIR")" ]; then
    mv $INTERNALPBALLDIR/* "$PBALLDIR"
#elif [ ! -f "$PBALLDIR/gamei386.so" ]; then
fi

mv "$INTERNALPBALLDIR/gamei386.so" "$PBALLDIR"

cd "$PBALLDIR/.."
./paintball2 "$@"
