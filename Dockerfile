FROM ubuntu:20.04
LABEL description="This image intends to ease the installation and operation of a Digital Paintball 2 server"
RUN apt-get update; apt-get upgrade -y;\
    apt-get install libc6-i386 wget -y;\
    useradd -ms /bin/bash dpserver;\
    wget -O - https://downloads.sourceforge.net/paintball2/paintball2_build046_linux_full.tar.gz | tar zxf - --directory /;\
    mkdir /home/dpserver/pball/;\
    chown -hR dpserver:dpserver /paintball2/;\
    chown -hR dpserver:dpserver /home/dpserver/pball/;\
    mv /paintball2/pball/* /home/dpserver/pball/

ADD start.sh /usr/bin/
USER dpserver
WORKDIR /paintball2
EXPOSE 27910/udp

ENTRYPOINT ["start.sh"]
CMD ["+set", "dedicated", "1", "+exec", "server.cfg", "+map", "midnight"] #Server didn't work correctly when starting it without a map
